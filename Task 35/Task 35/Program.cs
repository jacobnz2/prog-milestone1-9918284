﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter in 3 whole numbers. Add a number and press the <ENTER> key");
            Console.WriteLine();

            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            var c = int.Parse(Console.ReadLine());
            Console.WriteLine();

            Console.WriteLine("Based on the whole numbers you chose, the answers to the following five equations are...");
            Console.WriteLine();

            Console.WriteLine($"1.  ({a} - {b}) * {c}  = {(a - b) * c }");
            Console.WriteLine($"2.  {a} * {b} - {c} = {a * b - c}");
            Console.WriteLine($"3.  {a} / {b} * {c} = {a / b * c}");
            Console.WriteLine($"4.  {a} / ({b} - {c}) = { a / b - c}");
            Console.WriteLine($"5.  ({a} + {b}) * {c} = {(a + b) * c}");

            Console.ReadLine();

        }
    }
}
