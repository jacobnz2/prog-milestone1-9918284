﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_31
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Enter a number to see if it is divisible by 3 and 4 as a whole number");

            var userNumber = int.Parse(Console.ReadLine());
            var a = userNumber % 3;
            var b = userNumber % 4;

            if (a == 0)
            {
                if (b == 0)
                {
                    Console.WriteLine($"Great!  Your number {userNumber} is divisible by 3 and 4 as a whole number.");
                }
                else
                {
                    Console.WriteLine($"Guts!  Your number {userNumber} is not divisible by 3 and 4 as a whole number.");
                }
            }
            else
            {
                Console.WriteLine($"Guts!  Your number {userNumber} is not divisible by 3 and 4 as a whole number.");
            }

        }
    }
}
