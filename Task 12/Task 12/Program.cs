﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_12
{
    class Program
    {
        static void Main(string[] args)
        {

            var number = 0;
            Console.WriteLine("Please tell me your favourite number.");
            number = int.Parse(Console.ReadLine());

            if ((number & 1) == 0)
            {
                Console.WriteLine("Your favourite number is an even number.");
                Console.ReadLine();
            }

            else
                Console.WriteLine("Your favourite number is an odd number.");
            Console.ReadLine();

        }
    }
}
