﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_30
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Type in a number and press <ENTER>");
            var a = Console.ReadLine();
            var b = int.Parse(a);

            Console.WriteLine();
            Console.WriteLine("Great! Now type in another number and press <ENTER>");
            var c = Console.ReadLine();
            var d = int.Parse(c);
            Console.WriteLine();

            var e = (a + c);
            Console.WriteLine("OK!  I've got it!");
            Console.WriteLine($"If I just take the string {a} and add it with {c} we will get {e}");

            Console.WriteLine();

            var f = (b + d);
            Console.WriteLine($"But if I take the number {b} and add it with {d} we will get {f}");

            Console.ReadLine();

        }
    }
}
