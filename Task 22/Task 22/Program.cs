﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_22
{
    class Program
    {
        static void Main(string[] args)
        {

            var i = 0;
            var fv = new Dictionary<string, string>();

            fv.Add("Apple", "Fruit");
            fv.Add("Banana", "Fruit");
            fv.Add("Orange", "Fruit");
            fv.Add("Carrot", "Vegetable");
            fv.Add("Onion", "Vegetable");
            fv.Add("Potato", "Vegetable");

            foreach (var x in fv)
            {
                if (x.Value == "Fruit")
                {
                    i++;
                }
                
            }
            Console.WriteLine($"There are {i} items of fruit in the dictionary");
            Console.WriteLine("Apple");
            Console.WriteLine("Banana");
            Console.WriteLine("Orange");

        }
    }
}
