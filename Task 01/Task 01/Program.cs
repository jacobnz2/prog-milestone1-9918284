﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is your name?");
            var userName = Console.ReadLine();

            Console.WriteLine($"Thank you {userName}.  Now what is your age?");
            var age = int.Parse(Console.ReadLine());

            Console.WriteLine($"Ok, " + userName + ", so you are " + age + " years old.");
            Console.WriteLine("{0}, you wouldn't believe it but I am also {1} years old too!", userName, age);

            Console.ReadLine();
        }
    }
}
