﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_29
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("The quick brown fox jumped over the fence.");

            string name = "The quick brown fox jumped over the fence.";
            int count;

            count = name.Split(' ').Length;

            Console.WriteLine("The number of words in this phrase is " + count);
            Console.ReadLine();

        }
    }
}
