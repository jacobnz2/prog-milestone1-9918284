﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_13
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Please enter the price of three different beers.  The computer will then add up the sum of beer costs and add the 15% GST for you after you press the <ENTER> key");

            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());
            var c = double.Parse(Console.ReadLine());
            var d = 1.15;
            var e = Math.Round((a + b + c) * d, 2);

            Console.WriteLine($"Your total price for three beers is ${e} ");
            Console.ReadLine();

        }
    }
}
