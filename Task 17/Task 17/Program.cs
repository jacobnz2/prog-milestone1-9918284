﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_17
{
    class Program
    {
        static void Main(string[] args)
        {

            var names = new List<Tuple<string, int>>();
            names.Add(Tuple.Create("Jimmy Hendrix", 27));
            names.Add(Tuple.Create("Joe Satriani", 60));
            names.Add(Tuple.Create("Steve Vai", 56));

            foreach (var x in names)
            {
                Console.WriteLine($"{x.Item1} is a guitarist who is {x.Item2} years old");
            }

            Console.ReadLine();

        }
    }
}
