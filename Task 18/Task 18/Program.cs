﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_18
{
    class Program
    {
        static void Main(string[] args)
        {

            var names = new List<Tuple<string, string, int>>();
            names.Add(Tuple.Create("Manny Pacquiao", "December", 17));
            names.Add(Tuple.Create("Rocky Marciano", "September", 1));
            names.Add(Tuple.Create("Saul Álvarez", "July", 18));

            foreach (var x in names)
            {
                Console.WriteLine($"{x.Item1} is a professional boxer whos birthday is {x.Item2} {x.Item3}");
            }

            Console.ReadLine();

        }
    }
}
