﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_37
{
    class Program
    {
        static void Main(string[] args)
        {

            var hour = double.Parse("10");
            var contactHour = double.Parse("5");
            var credit = double.Parse("15");
            var semester = double.Parse("12");

            Console.WriteLine("Programming is a 15 credit paper that runs for 12 semesters.  With the given 5 hours of contact hours each week, and a recommended 10 hours of study per credit, how many hours a week should we be studying?");
            Console.WriteLine();
            Console.WriteLine($"{hour} * {credit} = {hour * credit}");
            Console.WriteLine($"{hour * credit} - {semester} * {contactHour} = {hour * credit - semester * contactHour}");
            Console.WriteLine($"{hour * credit} - {semester} * {contactHour} / {semester} = {(hour * credit - semester * contactHour) / semester}");

            Console.WriteLine();
            Console.WriteLine($"For a 15 credit paper you should be studying {(hour * credit - semester * contactHour) / semester} hours a week!");
            Console.ReadLine();

        }
    }
}
